package com.example.telegramm_db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelegrammDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelegrammDbApplication.class, args);

        System.out.println("Heoole Data Base for Telegramm!!");
    }

}
