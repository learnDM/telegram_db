package com.example.telegramm_db.controller;



import com.example.telegramm_db.model.TelegrammCity;
import com.example.telegramm_db.repository.CityRepository;
import com.example.telegramm_db.service.ServiceCity;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Log
@AllArgsConstructor
@Controller
@RequestMapping
public class TelegramController {  // port 8080



    private ServiceCity serviceCity; // Autowired by constructor


    @ResponseBody
    @GetMapping(path = "/city-{city}")
    public  TelegrammCity getTelegramCity(@PathVariable String city){
        return serviceCity.getByCity(city) ;
    }


//    @ResponseBody
//    @GetMapping(path = "/index")
//    public  String getIndex (){
//
//        return  "Hello it is return just text!";
//    }
//
//
//
//    @ResponseBody
//    @GetMapping(path = "/getcityByREST")
//    public  String getOneHome (){
//
//
//        String url = "http://localhost:8080/index";
//        log.info("dm -> url: " + url);
//
//        return  "Hello!";
//    }




}
