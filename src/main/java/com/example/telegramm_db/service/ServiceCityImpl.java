package com.example.telegramm_db.service;


import com.example.telegramm_db.model.TelegrammCity;
import com.example.telegramm_db.repository.CityRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ServiceCityImpl implements ServiceCity {

   private CityRepository cityRepository; // Autowired by constructor


    @Override
    public TelegrammCity getByCity(String city) {
        return cityRepository.getByCity(city);
    }


}
