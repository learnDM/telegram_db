package com.example.telegramm_db.service;

import com.example.telegramm_db.model.TelegrammCity;



 public interface ServiceCity {

        TelegrammCity getByCity(String city);

    }
