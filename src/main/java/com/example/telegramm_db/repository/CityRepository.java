package com.example.telegramm_db.repository;

import com.example.telegramm_db.model.TelegrammCity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel = "telegrammcity", path = "telegrammcity")
public interface CityRepository extends JpaRepository<TelegrammCity, Long> {


    TelegrammCity getByCity(String city);

}
